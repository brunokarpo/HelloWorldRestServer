package br.nom.brunokarpo.recursos;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorldResource {

	@GET
	public Response sayHello() {
		String output = "Ola mundo!";

		return Response.status(Response.Status.OK).entity(output).build();
	}

}
